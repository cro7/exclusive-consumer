package com.example.demo;

public class Settings {

    public static final String SETTING_TYPE = "type";
    public static final String SETTING_TYPE_VALUE_CONSUMER = "consumer";
    public static final String SETTING_TYPE_VALUE_PRODUCER = "producer";

    public static final String SETTING_NAME = "name";

    public static final String SETTING_EXCLUSIVE_CONSUMER = "exclusive";

    public static final String SETTING_PROTOCOL = "protocol";
    public static final String SETTING_PROTOCOL_VALUE_RABBITMQ = "rabbitmq";
    public static final String SETTING_PROTOCOL_VALUE_ACTIVEMQ = "activemq";

    public static final String EXCLUSIVE_QUEUE_NAME = "test.exclusive";
    public static final String NON_EXCLUSIVE_QUEUE_NAME = "test.non-exclusive";
}
