package com.example.demo;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.jms.activemq.ActiveMQProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.ConnectionFactory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.example.demo.Settings.*;

@Component
@ConditionalOnProperty(name = SETTING_PROTOCOL, havingValue = SETTING_PROTOCOL_VALUE_ACTIVEMQ)
@EnableConfigurationProperties(ActiveMQProperties.class)
public class ActiveMQ {

    @Bean
    public ConnectionFactory connectionFactory(ActiveMQProperties properties) {
        return new ActiveMQConnectionFactory(properties.getUser(), properties.getPassword(), properties.getBrokerUrl());
    }

    @Bean(name = "listenerContainerFactory")
    public JmsListenerContainerFactory<?> listenerContainerFactory(ConnectionFactory connectionFactory,
                                                                   DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConcurrency("1-1");
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @Configuration
    @ConditionalOnProperty(name = SETTING_TYPE, havingValue = SETTING_TYPE_VALUE_PRODUCER)
    class Producer {
        @Bean
        public ApplicationRunner runner(JmsTemplate jmsTemplate, @Value("${" + SETTING_EXCLUSIVE_CONSUMER + "}") boolean exclusiveConsumer, @Value("${" + SETTING_NAME + "}") String name) {
            String queue = exclusiveConsumer ? EXCLUSIVE_QUEUE_NAME : NON_EXCLUSIVE_QUEUE_NAME;
            return args -> {
                for (int i = 0; ; i++) {
                    System.out.println("%s: %s: %s: sending msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, queue, i));
                    jmsTemplate.convertAndSend(queue, i);
                    Thread.sleep(1000L);
                }
            };
        }
    }

    @Configuration
    @ConditionalOnProperty(name = SETTING_TYPE, havingValue = SETTING_TYPE_VALUE_CONSUMER)
    class Consumer {

        @Value("${" + SETTING_NAME + "}")
        private String name;

        @Component
        @ConditionalOnProperty(name = SETTING_EXCLUSIVE_CONSUMER, havingValue = "true")
        class ConsumerExclusive {

            @JmsListener(destination = EXCLUSIVE_QUEUE_NAME + "?consumer.exclusive=true")
            public void consumeExclusive(String input) throws InterruptedException {
                System.out.println("%s: %s: %s: reading msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, EXCLUSIVE_QUEUE_NAME, input));
                Thread.sleep(2000L);
            }
        }

        @Component
        @ConditionalOnProperty(name = SETTING_EXCLUSIVE_CONSUMER, havingValue = "false")
        class ConsumerNonExclusive {

            @JmsListener(destination = NON_EXCLUSIVE_QUEUE_NAME)
            public void consumeNonExclusive(String input) throws InterruptedException {
                System.out.println("%s: %s: %s: reading msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, NON_EXCLUSIVE_QUEUE_NAME, input));
                Thread.sleep(2000L);
            }
        }

    }

}
