package com.example.demo;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.example.demo.Settings.*;

@Component
@ConditionalOnProperty(name = SETTING_PROTOCOL, havingValue = SETTING_PROTOCOL_VALUE_RABBITMQ)
public class RabbitMQ {

    @Configuration
    @ConditionalOnProperty(name = SETTING_TYPE, havingValue = SETTING_TYPE_VALUE_PRODUCER)
    class Producer {
        @Bean
        public ApplicationRunner runner(RabbitTemplate rabbitTemplate, @Value("${" + SETTING_EXCLUSIVE_CONSUMER + "}") boolean exclusiveConsumer, @Value("${" + SETTING_NAME + "}") String name) {
            String queue = exclusiveConsumer ? EXCLUSIVE_QUEUE_NAME : NON_EXCLUSIVE_QUEUE_NAME;
            return args -> {
                for (int i = 0; ; i++) {
                    System.out.println("%s: %s: %s: sending msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, queue, i));
                    rabbitTemplate.convertAndSend(queue, i);
                    Thread.sleep(1000L);
                }
            };
        }
    }

    @Configuration
    @ConditionalOnProperty(name = SETTING_TYPE, havingValue = SETTING_TYPE_VALUE_CONSUMER)
    class Consumer {

        @Value("${" + SETTING_NAME + "}")
        private String name;

        @Component
        @ConditionalOnProperty(name = SETTING_EXCLUSIVE_CONSUMER, havingValue = "false")
        class ConsumerNonExclusive {
            @Bean
            public Queue notExclusiveQueue() {
                return QueueBuilder
                        .durable(NON_EXCLUSIVE_QUEUE_NAME)
                        .build();
            }

            @RabbitListener(queues = {NON_EXCLUSIVE_QUEUE_NAME})
            public void consumeNonExclusive(String input) throws InterruptedException {
                System.out.println("%s: %s: %s: reading msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, NON_EXCLUSIVE_QUEUE_NAME, input));
                Thread.sleep(2000L);
            }
        }

        @Component
        @ConditionalOnProperty(name = SETTING_EXCLUSIVE_CONSUMER, havingValue = "true")
        class ConsumerExclusive {
            @Bean
            public Queue exclusiveQueue() {
                return QueueBuilder
                        .durable(EXCLUSIVE_QUEUE_NAME)
                        .withArgument("x-single-active-consumer", true)
                        .build();
            }

            @RabbitListener(queues = {EXCLUSIVE_QUEUE_NAME})
            public void consumeExclusive(String input) throws InterruptedException {
                System.out.println("%s: %s: %s: reading msg: %s".formatted(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME), name, EXCLUSIVE_QUEUE_NAME, input));
                Thread.sleep(2000L);
            }
        }
    }
}
