## test scenario
Multiple message consumers connected to a common queue. Use exclusive to enable/disable "exclusive consumer" functionality so only a single
consumer is processing messages while the other one is in "standby".

More info here:

https://activemq.apache.org/exclusive-consumer

https://www.rabbitmq.com/consumers.html#single-active-consumer

### Example

Open 3 shells and run:

make sure to use proper technology by using -Dprotocol=rabbitmq or -Dprotocol=activemq

Consumer 1

    mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dprotocol=activemq -Dtype=consumer -Dname=consumer#1 -Dexclusive=true"

Consumer 2

    mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dprotocol=activemq -Dtype=consumer -Dname=consumer#2 -Dexclusive=true"

Producer

    mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dprotocol=activemq -Dtype=producer -Dname=producer#1 -Dexclusive=true"

you should see that only one consumer is reading messages. If you set exclusive to false (default) then both are processing messages.

When exclusive=true: kill the consumer that is currently reading from exclusive queue via ctr-c and validate that other consumer takes over